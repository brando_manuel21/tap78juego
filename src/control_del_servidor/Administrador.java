/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control_del_servidor;

import java.awt.event.ActionListener;
import java.io.*;
import java.net.*;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JTextArea;
import javax.swing.table.*;

/**
 *
 * @author Brando
 */
public class Administrador extends javax.swing.JFrame {

    public static final int PUERTO = 4321;

    public Administrador() {
        initComponents();
        datosIP.setLineWrap(true);
        setLocationRelativeTo(null);
        DefaultTableModel modelo = (DefaultTableModel) datosJugadores.getModel();
        int filas = modelo.getRowCount();
        for (int i = 0; i < filas; i++) {
            modelo.removeRow(0);
        }
    }

    public void addEventos(ActionListener o) {
        act_servidor.addActionListener(o);
        IniciarServidor.addActionListener(o);
        detenerservidor.addActionListener(o);
        salirdelpto.addActionListener(o);
        cmPto.addActionListener(o);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        datosIP = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        datosJugadores = new javax.swing.JTable();
        pnlOperacion = new javax.swing.JPanel();
        detenerservidor = new javax.swing.JButton();
        IniciarServidor = new javax.swing.JButton();
        act_servidor = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        mnConfiguracion = new javax.swing.JMenu();
        cmPto = new javax.swing.JMenuItem();
        separador = new javax.swing.JPopupMenu.Separator();
        salirdelpto = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        datosIP.setColumns(20);
        datosIP.setRows(5);
        datosIP.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Logs", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 2, 12))); // NOI18N
        jScrollPane1.setViewportView(datosIP);

        datosJugadores.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Nombre", "Dinero", "Perdido", "Ganado"
            }
        ));
        jScrollPane2.setViewportView(datosJugadores);

        pnlOperacion.setBorder(javax.swing.BorderFactory.createTitledBorder("OPERACION"));

        detenerservidor.setText("Detener Servidor");
        detenerservidor.setEnabled(false);
        detenerservidor.setName("detener"); // NOI18N
        detenerservidor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                detenerservidorActionPerformed(evt);
            }
        });

        IniciarServidor.setText("Iniciar Servidor");
        IniciarServidor.setName("iniciar"); // NOI18N
        IniciarServidor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IniciarServidorActionPerformed(evt);
            }
        });

        act_servidor.setText("Actualizar");
        act_servidor.setEnabled(false);
        act_servidor.setName("actualizar"); // NOI18N
        act_servidor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                act_servidorActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlOperacionLayout = new javax.swing.GroupLayout(pnlOperacion);
        pnlOperacion.setLayout(pnlOperacionLayout);
        pnlOperacionLayout.setHorizontalGroup(
            pnlOperacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOperacionLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlOperacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(act_servidor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(detenerservidor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(IniciarServidor, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlOperacionLayout.setVerticalGroup(
            pnlOperacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOperacionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(IniciarServidor)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(detenerservidor)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(act_servidor)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        mnConfiguracion.setText("Configuración");

        cmPto.setText("Cambiar Puerto");
        cmPto.setName("puerto"); // NOI18N
        cmPto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmPtoActionPerformed(evt);
            }
        });
        mnConfiguracion.add(cmPto);
        mnConfiguracion.add(separador);

        salirdelpto.setText("Salir");
        salirdelpto.setName("salir"); // NOI18N
        salirdelpto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirdelptoActionPerformed(evt);
            }
        });
        mnConfiguracion.add(salirdelpto);

        jMenuBar1.add(mnConfiguracion);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 604, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(pnlOperacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(44, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 362, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(163, 163, 163)
                        .addComponent(pnlOperacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmPtoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmPtoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmPtoActionPerformed

    private void IniciarServidorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IniciarServidorActionPerformed


    }//GEN-LAST:event_IniciarServidorActionPerformed

    private void detenerservidorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_detenerservidorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_detenerservidorActionPerformed

    private void act_servidorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_act_servidorActionPerformed

    }//GEN-LAST:event_act_servidorActionPerformed

    private void salirdelptoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salirdelptoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_salirdelptoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton IniciarServidor;
    private javax.swing.JButton act_servidor;
    private javax.swing.JMenuItem cmPto;
    public javax.swing.JTextArea datosIP;
    private javax.swing.JTable datosJugadores;
    private javax.swing.JButton detenerservidor;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JMenu mnConfiguracion;
    private javax.swing.JPanel pnlOperacion;
    private javax.swing.JMenuItem salirdelpto;
    private javax.swing.JPopupMenu.Separator separador;
    // End of variables declaration//GEN-END:variables

    /**
     * @return the jTable1
     */
    public javax.swing.JTable getjTable1() {
        return datosJugadores;
    }

    public JButton getBotonActualizar() {
        return act_servidor;
    }

    public JButton getBotonDetener() {
        return detenerservidor;
    }

    public JButton getBotonIniciar() {
        return IniciarServidor;
    }

    public JTextArea getAreaLog() {
        return datosIP;
    }

    public JMenuItem getItemPuerto() {
        return cmPto;
    }
    
    

}
