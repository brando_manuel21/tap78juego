package Juego_Cliente;

import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.ImageIcon;
import javax.swing.JComponent;

public class JFrutas extends JComponent {

    private ImageIcon image = new ImageIcon(getClass().getResource("/game/resource/figuras.jpg"));
    private int y = 0;
    private Dimension d = new Dimension(100, 100);

    /**
     * Constructor de clase
     */
    JFrutas() {
        setSize(d);
        setPreferredSize(d);
        setVisible(true);
    }

    /**
     * Actualiza coordenada en Y
     */
    public void updateY(int value) {
        y = value;
        repaint();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.drawImage(image.getImage(), 0, y, 100, 400, this);
        g.dispose();
    }

}
