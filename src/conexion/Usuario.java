/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Brando
 */
public class Usuario {

    private final String TABLA = "usuario";
    private final String SQL_INSERT = "INSERT INTO " + TABLA + " (usuario,contra,jugador_idjugador) values(?,?,?)";
    private final String SQL_DELETE = "DELETE FROM " + TABLA + " where id=?";
    private final String SQL_QUERY = " SELECT * FROM " + TABLA + " where id=?";
    private final String SQL_UPDATE = "UPDATE " + TABLA + " set usuario=?, contra=?, jugador_idjugador=? where id=?";

    public boolean insertarUsuario(String usuario, String contra, String jugador_idjugador) {
        Connection con = null;
        PreparedStatement st = null;
        try {
            con = Conexion.getConnection();
            st = con.prepareStatement(SQL_INSERT);
            st.setString(1, usuario);
            st.setString(2, contra);
            st.setString(3, jugador_idjugador);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println("Error al insertar datos del usuario " + e);
            return false;
        } finally {
            Conexion.close(con);
            Conexion.close(st);
        }
        return true;
    }

    public boolean eliminarUsuario(String id) {
        Connection con = null;
        PreparedStatement st = null;
        try {
            con = Conexion.getConnection();
            st = con.prepareStatement(SQL_DELETE);
            st.setString(1, id);
            int num = st.executeUpdate();
            if (num == 0) {
                return false;
            }
        } catch (Exception e) {
            System.out.println("Error al eliminar al usuario " + e);
            return false;
        } finally {
            Conexion.close(con);
            Conexion.close(st);
        }
        return true;
    }

    public UsuarioBCKR consultaUsuario(String id) {
        Connection con = null;
        PreparedStatement st = null;
        UsuarioBCKR pojo = new UsuarioBCKR();
        try {
            con = Conexion.getConnection();
            st = con.prepareStatement(SQL_QUERY);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                pojo.setIdusuario(rs.getInt("idjugador")); 
                pojo.setUsuario(rs.getString("nombre"));
                pojo.setContra(rs.getString("apellidos"));
                pojo.setFecha(rs.getDate("fecha"));
                pojo.setIdJugador(rs.getInt("jugador_idjugador"));
            }
            Conexion.close(rs);
        } catch (Exception e) {
            System.out.println("Error al consultar " + e);
        } finally {
            Conexion.close(con);
            Conexion.close(st);
        }
        return pojo;
    }

    public boolean actualizarUsuario(String id, String usuario, String contra, String jugador_idjugador) {
        Connection con = null;
        PreparedStatement st = null;
        try {
            con = Conexion.getConnection();
            st = con.prepareStatement(SQL_UPDATE);
            st.setString(1, usuario);
            st.setString(2, contra);
            st.setString(3, jugador_idjugador);
            st.setString(4, id);
            int i = st.executeUpdate();
            if (i == 0) {
                return false;
            }
        } catch (Exception e) {
            System.out.println("Error al actualizar datos del usuario " + e);
            return false;
        } finally {
            Conexion.close(con);
            Conexion.close(st);
        }
        return true;
    }

}
